﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="common rt/pc" Type="Folder">
			<Item Name="cmw-property_configuration.ctl" Type="VI" URL="../../../src/common/cmw-property_configuration.ctl"/>
			<Item Name="cmw-property_temperature-acq-trigger.ctl" Type="VI" URL="../../../src/common/cmw-property_temperature-acq-trigger.ctl"/>
			<Item Name="cmw-property_temperature.ctl" Type="VI" URL="../../../src/common/cmw-property_temperature.ctl"/>
			<Item Name="cmw-property_voltage-acq-trigger.ctl" Type="VI" URL="../../../src/common/cmw-property_voltage-acq-trigger.ctl"/>
			<Item Name="cmw-property_voltage.ctl" Type="VI" URL="../../../src/common/cmw-property_voltage.ctl"/>
		</Item>
		<Item Name="pc" Type="Folder">
			<Item Name="cmw-set.vi" Type="VI" URL="../../../src/pc/cmw-set.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Array of VData to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Array of VData to VCluster__ogtk.vi"/>
				<Item Name="Array Size(s)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Array Size(s)__ogtk.vi"/>
				<Item Name="Array to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Array to Array of VData__ogtk.vi"/>
				<Item Name="Array to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Array to VCluster__ogtk.vi"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="cmw_copies.lvlib" Type="Library" URL="/&lt;userlib&gt;/_RADE/cmw/common_vis_ctl_from_other/cmw_copies.lvlib"/>
				<Item Name="cmw_ref_cluster.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/cmw/classes/CMWSubscribe/cmw_ref_cluster.ctl"/>
				<Item Name="CMWBase.lvclass" Type="LVClass" URL="/&lt;userlib&gt;/_RADE/cmw/classes/CMWBase/CMWBase.lvclass"/>
				<Item Name="CMWSet.lvclass" Type="LVClass" URL="/&lt;userlib&gt;/_RADE/cmw/classes/CMWSet/CMWSet.lvclass"/>
				<Item Name="CMWSubscribe.lvclass" Type="LVClass" URL="/&lt;userlib&gt;/_RADE/cmw/classes/CMWSubscribe/CMWSubscribe.lvclass"/>
				<Item Name="Command-SubscriptionMonitor.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/cmw/controls/Command-SubscriptionMonitor.ctl"/>
				<Item Name="Data-AsynchronousSet.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/cmw/controls/Data-AsynchronousSet.ctl"/>
				<Item Name="Delete Elements from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (I8)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (I16)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (I32)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (I64)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (Path)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (U8)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (U16)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (U32)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (U64)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (I8)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (I16)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (I32)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (I64)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (Path)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (U8)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (U16)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (U32)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (U64)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from 2D Array (Variant)__ogtk.vi"/>
				<Item Name="Delete Elements from Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Delete Elements from Array__ogtk.vi"/>
				<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
				<Item Name="Get Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Data Name__ogtk.vi"/>
				<Item Name="Get Default Data from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Default Data from TD__ogtk.vi"/>
				<Item Name="Get Element TD from Array TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Element TD from Array TD__ogtk.vi"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get TDEnum from Data__ogtk.vi"/>
				<Item Name="liblist.lvlib" Type="Library" URL="/&lt;userlib&gt;/_RADE/mta-lib/liblist/liblist.lvlib"/>
				<Item Name="mta_utils.lvlib" Type="Library" URL="/&lt;userlib&gt;/_RADE/mta-lib/utils/mta_utils.lvlib"/>
				<Item Name="Reorder 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (CXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (DBL)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (EXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (I8)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (I16)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (I32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (I64)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (Path)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (SGL)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (String)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (U8)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (U16)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (U32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (U64)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 1D Array2 (Variant)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (CDB)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (CSG)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (CXT)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (DBL)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (EXT)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (I8)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (I16)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (I32)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (I64)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (Path)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (SGL)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (String)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (U8)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (U16)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (U32)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (U64)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder 2D Array2 (Variant)__ogtk.vi"/>
				<Item Name="Reorder Array2__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Reorder Array2__ogtk.vi"/>
				<Item Name="Reshape Array to 1D VArray__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Reshape Array to 1D VArray__ogtk.vi"/>
				<Item Name="RIO-CMWWrapper.dll" Type="Document" URL="/&lt;userlib&gt;/_RADE/libs/RIO-CMWWrapper.dll"/>
				<Item Name="Search 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Search 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Search 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Search 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Search 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Search 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Search 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (I8)__ogtk.vi"/>
				<Item Name="Search 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (I16)__ogtk.vi"/>
				<Item Name="Search 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (I32)__ogtk.vi"/>
				<Item Name="Search 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (I64)__ogtk.vi"/>
				<Item Name="Search 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Search 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (Path)__ogtk.vi"/>
				<Item Name="Search 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Search 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (String)__ogtk.vi"/>
				<Item Name="Search 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (U8)__ogtk.vi"/>
				<Item Name="Search 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (U16)__ogtk.vi"/>
				<Item Name="Search 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (U32)__ogtk.vi"/>
				<Item Name="Search 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (U64)__ogtk.vi"/>
				<Item Name="Search 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Search Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Search Array__ogtk.vi"/>
				<Item Name="Set Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Set Data Name__ogtk.vi"/>
				<Item Name="Sort 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (String)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 1D Array (U64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (String)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort 2D Array (U64)__ogtk.vi"/>
				<Item Name="Sort Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/array/array.llb/Sort Array__ogtk.vi"/>
				<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
				<Item Name="StateMachine-CMWAsynchronousSet.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/cmw/controls/StateMachine-CMWAsynchronousSet.ctl"/>
				<Item Name="StateMachine-CMWOpen.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/cmw/controls/StateMachine-CMWOpen.ctl"/>
				<Item Name="StateMachine-CMWOpenSubscription.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/cmw/controls/StateMachine-CMWOpenSubscription.ctl"/>
				<Item Name="StateMachine-CMWSet.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/cmw/controls/StateMachine-CMWSet.ctl"/>
				<Item Name="StateMachine-CMWSubscription.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/cmw/controls/StateMachine-CMWSubscription.ctl"/>
				<Item Name="StateMachine-SubscriptionListener.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/cmw/controls/StateMachine-SubscriptionListener.ctl"/>
				<Item Name="SubscriptionId.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/cmw/controls/SubscriptionId.ctl"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="CMW Server" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{8A645BD7-F52E-4DB6-8C2C-93820BF69642}</Property>
				<Property Name="App_INI_GUID" Type="Str">{4A82C1D1-0742-4E9D-8CD1-87D9B2F977B5}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{ABF928F0-0979-460B-BC3A-3C2728E81FA4}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CMW Server</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/workspace/builds/cmw server</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{FBB4A52C-3271-4F9F-8BA5-844C927D8A1A}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">CMW Server.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/workspace/builds/cmw server/CMW Server.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/workspace/builds/cmw server/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{F6DA58AC-5CF6-4145-AC0B-1EBFE2355D34}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">AGH University of Science and Technology</Property>
				<Property Name="TgtF_fileDescription" Type="Str">CMW Server</Property>
				<Property Name="TgtF_internalName" Type="Str">CMW Server</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2022 AGH University of Science and Technology</Property>
				<Property Name="TgtF_productName" Type="Str">CMW Server</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{169C43B6-5793-487A-88E1-B5D192AC6569}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CMW Server.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="RT PXI Target" Type="RT PXI Chassis">
		<Property Name="alias.name" Type="Str">RT PXI Target</Property>
		<Property Name="alias.value" Type="Str">cfx-864-mta005</Property>
		<Property Name="CCSymbols" Type="Str">TARGET_TYPE,RT;OS,Linux;CPU,x64;</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">9</Property>
		<Property Name="host.TargetOSID" Type="UInt">19</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str"></Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="Int">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">false</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/home/lvuser/natinst/bin</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.WebServer.Config" Type="Str">Listen 8000

NI.ServerName default
DocumentRoot "$LVSERVER_DOCROOT"
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
DirectoryIndex index.htm
WorkerLimit 10
InactivityTimeout 60

LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule

#
# Pipeline Definition
#

SetConnector netConnector

AddHandler LVAuth
AddHandler LVRFP

AddHandler fileHandler ""

AddOutputFilter chunkFilter


</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="common rt/pc" Type="Folder">
			<Item Name="cmw-property_configuration.ctl" Type="VI" URL="../../../src/common/cmw-property_configuration.ctl"/>
			<Item Name="cmw-property_temperature-acq-trigger.ctl" Type="VI" URL="../../../src/common/cmw-property_temperature-acq-trigger.ctl"/>
			<Item Name="cmw-property_temperature.ctl" Type="VI" URL="../../../src/common/cmw-property_temperature.ctl"/>
			<Item Name="cmw-property_voltage-acq-trigger.ctl" Type="VI" URL="../../../src/common/cmw-property_voltage-acq-trigger.ctl"/>
			<Item Name="cmw-property_voltage.ctl" Type="VI" URL="../../../src/common/cmw-property_voltage.ctl"/>
		</Item>
		<Item Name="rt" Type="Folder">
			<Item Name="### top level ###" Type="Folder">
				<Item Name="rt-cam-acq.vi" Type="VI" URL="../../../src/rt/rt-cam-acq.vi"/>
				<Item Name="rt-main.vi" Type="VI" URL="../../../src/rt/rt-main.vi"/>
			</Item>
			<Item Name="subVIs" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="communication" Type="Folder">
					<Item Name="rt-close-communication.vi" Type="VI" URL="../../../src/rt/subVIs/communication/rt-close-communication.vi"/>
					<Item Name="rt-communication.ctl" Type="VI" URL="../../../src/rt/subVIs/communication/rt-communication.ctl"/>
					<Item Name="rt-init-communication.vi" Type="VI" URL="../../../src/rt/subVIs/communication/rt-init-communication.vi"/>
					<Item Name="rt-communication_queues.ctl" Type="VI" URL="../../../src/rt/subVIs/communication/rt-communication_queues.ctl"/>
					<Item Name="rt-communication_user-events.ctl" Type="VI" URL="../../../src/rt/subVIs/communication/rt-communication_user-events.ctl"/>
				</Item>
				<Item Name="config" Type="Folder">
					<Item Name="rt-acq-configuration.ctl" Type="VI" URL="../../../src/rt/subVIs/config/rt-acq-configuration.ctl"/>
					<Item Name="rt-configuration.ctl" Type="VI" URL="../../../src/rt/subVIs/config/rt-configuration.ctl"/>
					<Item Name="rt-create-config-dir-if-non-existant.vi" Type="VI" URL="../../../src/rt/subVIs/config/rt-create-config-dir-if-non-existant.vi"/>
					<Item Name="rt-get-config-path.vi" Type="VI" URL="../../../src/rt/subVIs/config/rt-get-config-path.vi"/>
					<Item Name="rt-read-configuration.vi" Type="VI" URL="../../../src/rt/subVIs/config/rt-read-configuration.vi"/>
					<Item Name="rt-write-configuration.vi" Type="VI" URL="../../../src/rt/subVIs/config/rt-write-configuration.vi"/>
					<Item Name="rt-log-configuration.vi" Type="VI" URL="../../../src/rt/subVIs/config/rt-log-configuration.vi"/>
					<Item Name="rt-read-config-as-text.vi" Type="VI" URL="../../../src/rt/subVIs/config/rt-read-config-as-text.vi"/>
				</Item>
				<Item Name="temperature module" Type="Folder">
					<Item Name="rt-temperature_main.vi" Type="VI" URL="../../../src/rt/subVIs/temperature module/rt-temperature_main.vi"/>
				</Item>
				<Item Name="voltage module" Type="Folder">
					<Item Name="rt-voltage_main.vi" Type="VI" URL="../../../src/rt/subVIs/voltage module/rt-voltage_main.vi"/>
				</Item>
				<Item Name="main" Type="Folder">
					<Item Name="rt-main-loop.vi" Type="VI" URL="../../../src/rt/subVIs/main loop/rt-main-loop.vi"/>
					<Item Name="rt-main-loop_state.ctl" Type="VI" URL="../../../src/rt/subVIs/main loop/rt-main-loop_state.ctl"/>
					<Item Name="rt-main-loop_module-data.ctl" Type="VI" URL="../../../src/rt/subVIs/main loop/rt-main-loop_module-data.ctl"/>
					<Item Name="rt-main-loop_validate-configuration.vi" Type="VI" URL="../../../src/rt/subVIs/main loop/rt-main-loop_validate-configuration.vi"/>
					<Item Name="rt-main-loop_validate-configuration-steps.ctl" Type="VI" URL="../../../src/rt/subVIs/main loop/rt-main-loop_validate-configuration-steps.ctl"/>
					<Item Name="rt-main_accept-set-action.vi" Type="VI" URL="../../../src/rt/subVIs/main loop/rt-main_accept-set-action.vi"/>
					<Item Name="rt-main-loop_reject-set-action.vi" Type="VI" URL="../../../src/rt/subVIs/main loop/rt-main-loop_reject-set-action.vi"/>
					<Item Name="rt-main-loop_validate-temperature-trigger.vi" Type="VI" URL="../../../src/rt/subVIs/main loop/rt-main-loop_validate-temperature-trigger.vi"/>
					<Item Name="rt-main-loop_validate-voltage-trigger.vi" Type="VI" URL="../../../src/rt/subVIs/main loop/rt-main-loop_validate-voltage-trigger.vi"/>
				</Item>
				<Item Name="daq module" Type="Folder">
					<Item Name="API" Type="Folder">
						<Item Name="controls" Type="Folder">
							<Item Name="daq-module_queue-msg.ctl" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_queue-msg.ctl"/>
							<Item Name="daq-module_module-data.ctl" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_module-data.ctl"/>
						</Item>
						<Item Name="daq-module_close.vi" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_close.vi"/>
						<Item Name="daq-module_init.vi" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_init.vi"/>
						<Item Name="daq-module_main.vi" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_main.vi"/>
						<Item Name="daq-module_settings.ctl" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_settings.ctl"/>
					</Item>
					<Item Name="subVIs" Type="Folder">
						<Item Name="daq-module_main-loop-state.ctl" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_main-loop-state.ctl"/>
						<Item Name="daq-module_internal-communication.ctl" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_internal-communication.ctl"/>
						<Item Name="daq-module_data-sample.ctl" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_data-sample.ctl"/>
						<Item Name="daq-module_module-data-internal.ctl" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_module-data-internal.ctl"/>
						<Item Name="daq-module_acq-loop-state.ctl" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_acq-loop-state.ctl"/>
						<Item Name="daq-module_msg-type.ctl" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_msg-type.ctl"/>
						<Item Name="daq-module_module-communication.ctl" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_module-communication.ctl"/>
						<Item Name="daq-module_external-communication.ctl" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_external-communication.ctl"/>
						<Item Name="daq-module_close-internal-communication.vi" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_close-internal-communication.vi"/>
						<Item Name="daq-module_init-internal-communication.vi" Type="VI" URL="../../../src/rt/subVIs/daq module/daq-module_init-internal-communication.vi"/>
					</Item>
				</Item>
				<Item Name="cmw server" Type="Folder">
					<Item Name="subVIs" Type="Folder">
						<Item Name="cmw-server_accept-set-request-msg.ctl" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_accept-set-request-msg.ctl"/>
						<Item Name="cmw-server_msg-type.ctl" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_msg-type.ctl"/>
						<Item Name="cmw-server_queue-msg.ctl" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_queue-msg.ctl"/>
						<Item Name="cmw-server_reject-set-request-msg.ctl" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_reject-set-request-msg.ctl"/>
						<Item Name="cmw-server_set-request.ctl" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_set-request.ctl"/>
						<Item Name="cmw-server_listener.vi" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_listener.vi"/>
						<Item Name="cmw-server_launch-listener.vi" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_launch-listener.vi"/>
						<Item Name="cmw-server_property-settings.ctl" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_property-settings.ctl"/>
						<Item Name="cmw-server_add-property.vi" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_add-property.vi"/>
						<Item Name="cmw-server_init.vi" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_init.vi"/>
						<Item Name="cmw-server_check-if-cmw-available.vi" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_check-if-cmw-available.vi"/>
						<Item Name="cmw-server_loop-state.ctl" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_loop-state.ctl"/>
						<Item Name="cmw-server_module-data.ctl" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server_module-data.ctl"/>
					</Item>
					<Item Name="cmw-server.vi" Type="VI" URL="../../../src/rt/subVIs/cmw server/cmw-server.vi"/>
				</Item>
				<Item Name="rt-init.vi" Type="VI" URL="../../../src/rt/subVIs/rt-init.vi"/>
				<Item Name="rt-error-report.ctl" Type="VI" URL="../../../src/rt/subVIs/rt-error-report.ctl"/>
				<Item Name="rt-close.vi" Type="VI" URL="../../../src/rt/subVIs/rt-close.vi"/>
				<Item Name="rt-application-data.ctl" Type="VI" URL="../../../src/rt/subVIs/rt-application-data.ctl"/>
			</Item>
		</Item>
		<Item Name="cam_init.vi" Type="VI" URL="../../../src/rt/subVIs/cam_init.vi"/>
		<Item Name="stop_yn" Type="VI" URL="../../../src/rt/subVIs/stop_yn"/>
		<Item Name="trigger.vi" Type="VI" URL="../../../src/rt/subVIs/trigger.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Array of VData to VArray__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Array of VData to VArray__ogtk.vi"/>
				<Item Name="Array of VData to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Array of VData to VCluster__ogtk.vi"/>
				<Item Name="Array Size(s)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Array Size(s)__ogtk.vi"/>
				<Item Name="Array to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Array to Array of VData__ogtk.vi"/>
				<Item Name="Array to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Array to VCluster__ogtk.vi"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Build Path - File Names and Paths Arrays - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/file/file.llb/Build Path - File Names and Paths Arrays - path__ogtk.vi"/>
				<Item Name="Build Path - File Names and Paths Arrays__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/file/file.llb/Build Path - File Names and Paths Arrays__ogtk.vi"/>
				<Item Name="Build Path - File Names Array - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/file/file.llb/Build Path - File Names Array - path__ogtk.vi"/>
				<Item Name="Build Path - File Names Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/file/file.llb/Build Path - File Names Array__ogtk.vi"/>
				<Item Name="Build Path - Traditional - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/file/file.llb/Build Path - Traditional - path__ogtk.vi"/>
				<Item Name="Build Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/file/file.llb/Build Path - Traditional__ogtk.vi"/>
				<Item Name="Build Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/file/file.llb/Build Path__ogtk.vi"/>
				<Item Name="Cluster to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Cluster to Array of VData__ogtk.vi"/>
				<Item Name="CMWSDeleteDevices.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rio-cmwwrapper/CMWSDeleteDevices.vi"/>
				<Item Name="CMWServer.lvclass" Type="LVClass" URL="/&lt;userlib&gt;/_RADE/rio-cmwwrapper/CMWServer.lvclass"/>
				<Item Name="Create Dir if Non-Existant__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/file/file.llb/Create Dir if Non-Existant__ogtk.vi"/>
				<Item Name="Encode Section and Key Names__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/variantconfig/variantconfig.llb/Encode Section and Key Names__ogtk.vi"/>
				<Item Name="Format Numeric Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/variantconfig/variantconfig.llb/Format Numeric Array__ogtk.vi"/>
				<Item Name="Format Variant Into String__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/string/string.llb/Format Variant Into String__ogtk.vi"/>
				<Item Name="Get Array Element TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Array Element TD__ogtk.vi"/>
				<Item Name="Get Array Element TDEnum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Array Element TDEnum__ogtk.vi"/>
				<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
				<Item Name="Get Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Data Name__ogtk.vi"/>
				<Item Name="Get Default Data from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Default Data from TD__ogtk.vi"/>
				<Item Name="Get Element TD from Array TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Element TD from Array TD__ogtk.vi"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Strings from Enum TD__ogtk.vi"/>
				<Item Name="Get Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get Strings from Enum__ogtk.vi"/>
				<Item Name="Get TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Get TDEnum from Data__ogtk.vi"/>
				<Item Name="libCMWSWrapper.dll" Type="Document" URL="/&lt;userlib&gt;/_RADE/libs/libCMWSWrapper.dll"/>
				<Item Name="Parse String with TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Parse String with TDs__ogtk.vi"/>
				<Item Name="Periodic Trigger__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/time/time.llb/Periodic Trigger__ogtk.vi"/>
				<Item Name="RADE-logger-log.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/public/RADE-logger-log.vi"/>
				<Item Name="Read INI Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/variantconfig/variantconfig.llb/Read INI Cluster__ogtk.vi"/>
				<Item Name="Read Key (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/variantconfig/variantconfig.llb/Read Key (Variant)__ogtk.vi"/>
				<Item Name="Read Section Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/variantconfig/variantconfig.llb/Read Section Cluster__ogtk.vi"/>
				<Item Name="Reshape 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Reshape 1D Array__ogtk.vi"/>
				<Item Name="Reshape Array to 1D VArray__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Reshape Array to 1D VArray__ogtk.vi"/>
				<Item Name="RL-close-FGV_refs.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/init-close/RL-close-FGV_refs.vi"/>
				<Item Name="RL-close-main.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/init-close/RL-close-main.vi"/>
				<Item Name="RL-close-user_events.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/init-close/RL-close-user_events.vi"/>
				<Item Name="RL-common-clear_error.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/common/RL-common-clear_error.vi"/>
				<Item Name="RL-common-common_clust-FGV.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/common/RL-common-common_clust-FGV.vi"/>
				<Item Name="RL-common-data.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/common/RL-common-data.ctl"/>
				<Item Name="RL-common-get_other_time_stamp.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/common/RL-common-get_other_time_stamp.vi"/>
				<Item Name="RL-common-is_log_initiated.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/common/RL-common-is_log_initiated.vi"/>
				<Item Name="RL-common-is_there_any_input.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/common/RL-common-is_there_any_input.vi"/>
				<Item Name="RL-common-ISO8601_time_string.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/common/RL-common-ISO8601_time_string.vi"/>
				<Item Name="RL-common-settings_data.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/common/RL-common-settings_data.ctl"/>
				<Item Name="RL-common-time_intervale.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/common/RL-common-time_intervale.ctl"/>
				<Item Name="RL-common-user_events.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/common/RL-common-user_events.ctl"/>
				<Item Name="RL-daemon-check_for_remote_settings.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/daemon/RL-daemon-check_for_remote_settings.vi"/>
				<Item Name="RL-daemon-enable_user_event.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/daemon/RL-daemon-enable_user_event.vi"/>
				<Item Name="RL-daemon-main.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/daemon/RL-daemon-main.vi"/>
				<Item Name="RL-daemon-register_user_event.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/daemon/RL-daemon-register_user_event.vi"/>
				<Item Name="RL-daemon-stop_core.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/daemon/RL-daemon-stop_core.vi"/>
				<Item Name="RL-init-FGV_refs.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/init-close/RL-init-FGV_refs.vi"/>
				<Item Name="RL-init-init_if_not_running.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/init-close/RL-init-init_if_not_running.vi"/>
				<Item Name="RL-init-launch_daemon.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/init-close/RL-init-launch_daemon.vi"/>
				<Item Name="RL-init-main.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/init-close/RL-init-main.vi"/>
				<Item Name="RL-init-user_events.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/init-close/RL-init-user_events.vi"/>
				<Item Name="RL-log-backup_log_file.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-backup_log_file.vi"/>
				<Item Name="RL-log-find_module_from_call_chain.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-find_module_from_call_chain.vi"/>
				<Item Name="RL-log-format_and_write.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-format_and_write.vi"/>
				<Item Name="RL-log-format_msg.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-format_msg.vi"/>
				<Item Name="RL-log-handle_input_error.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-handle_input_error.vi"/>
				<Item Name="RL-log-is_file_too_large.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-is_file_too_large.vi"/>
				<Item Name="RL-log-is_file_too_old.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-is_file_too_old.vi"/>
				<Item Name="RL-log-is_there_too many_backups.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-is_there_too many_backups.vi"/>
				<Item Name="RL-log-log_adpt_to_input.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/interface/RL-log-log_adpt_to_input.vi"/>
				<Item Name="RL-log-log_critical_error.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/interface/RL-log-log_critical_error.vi"/>
				<Item Name="RL-log-log_debug.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/interface/RL-log-log_debug.vi"/>
				<Item Name="RL-log-log_error.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/interface/RL-log-log_error.vi"/>
				<Item Name="RL-log-log_info.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/interface/RL-log-log_info.vi"/>
				<Item Name="RL-log-log_level_enum.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-log_level_enum.ctl"/>
				<Item Name="RL-log-log_locally.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-log_locally.vi"/>
				<Item Name="RL-log-log_online.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-log_online.vi"/>
				<Item Name="RL-log-log_self_error.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-log_self_error.vi"/>
				<Item Name="RL-log-log_warning.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/interface/RL-log-log_warning.vi"/>
				<Item Name="RL-log-logger_input.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-logger_input.ctl"/>
				<Item Name="RL-log-send_log_to_minion.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-send_log_to_minion.vi"/>
				<Item Name="RL-log-write_to_file.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/log/RL-log-write_to_file.vi"/>
				<Item Name="RL-settings-check_directory_permission.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/vi/RL-settings-check_directory_permission.vi"/>
				<Item Name="RL-settings-create_file_if_no_file.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/vi/RL-settings-create_file_if_no_file.vi"/>
				<Item Name="RL-settings-default_paths.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/vi/RL-settings-default_paths.vi"/>
				<Item Name="RL-settings-default_values.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/vi/RL-settings-default_values.vi"/>
				<Item Name="RL-settings-fill_module_with_missing_settings.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/vi/RL-settings-fill_module_with_missing_settings.vi"/>
				<Item Name="RL-settings-generate_default_file.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/vi/RL-settings-generate_default_file.vi"/>
				<Item Name="RL-settings-get_settings.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/vi/RL-settings-get_settings.vi"/>
				<Item Name="RL-settings-log_level_bool.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/controls/RL-settings-log_level_bool.ctl"/>
				<Item Name="RL-settings-option_clust.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/controls/RL-settings-option_clust.ctl"/>
				<Item Name="RL-settings-read_file.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/vi/RL-settings-read_file.vi"/>
				<Item Name="RL-settings-save_format.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/controls/RL-settings-save_format.ctl"/>
				<Item Name="RL-settings-save_format_module.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/controls/RL-settings-save_format_module.ctl"/>
				<Item Name="RL-settings-settings_types.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/controls/RL-settings-settings_types.ctl"/>
				<Item Name="RL-settings-storage_clust.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/controls/RL-settings-storage_clust.ctl"/>
				<Item Name="RL-settings-storage_time_interval.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/controls/RL-settings-storage_time_interval.ctl"/>
				<Item Name="RL-settings-UDP_clust.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/controls/RL-settings-UDP_clust.ctl"/>
				<Item Name="RL-settings-write_to_file.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/setting/vi/RL-settings-write_to_file.vi"/>
				<Item Name="RL-UDP-close_config_port.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/UDP/RL-UDP-close_config_port.vi"/>
				<Item Name="RL-UDP-clust.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/UDP/RL-UDP-clust.ctl"/>
				<Item Name="RL-UDP-get_config.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/UDP/RL-UDP-get_config.vi"/>
				<Item Name="RL-UDP-open_config_port.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/UDP/RL-UDP-open_config_port.vi"/>
				<Item Name="RL-UDP-set_log.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/rade-logger/log_client/modules/UDP/RL-UDP-set_log.vi"/>
				<Item Name="Set Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Set Data Name__ogtk.vi"/>
				<Item Name="Set Enum String Value__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Set Enum String Value__ogtk.vi"/>
				<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
				<Item Name="Strip Path - Arrays__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/file/file.llb/Strip Path - Arrays__ogtk.vi"/>
				<Item Name="Strip Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/file/file.llb/Strip Path - Traditional__ogtk.vi"/>
				<Item Name="Strip Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/file/file.llb/Strip Path__ogtk.vi"/>
				<Item Name="Strip Units__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Strip Units__ogtk.vi"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
				<Item Name="Write INI Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/variantconfig/variantconfig.llb/Write INI Cluster__ogtk.vi"/>
				<Item Name="Write Key (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/variantconfig/variantconfig.llb/Write Key (Variant)__ogtk.vi"/>
				<Item Name="Write Section Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_RADE/mta-lib/variantconfig/variantconfig.llb/Write Section Cluster__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Equal Comparable.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Equal/Equal Comparable/Equal Comparable.lvclass"/>
				<Item Name="Equal Functor.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Equal/Equal Functor/Equal Functor.lvclass"/>
				<Item Name="Equals.vim" Type="VI" URL="/&lt;vilib&gt;/Comparison/Equals.vim"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Image Type" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Image Type"/>
				<Item Name="IMAQ Create" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Create"/>
				<Item Name="IMAQ Image.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Image.ctl"/>
				<Item Name="IMAQdx.ctl" Type="VI" URL="/&lt;vilib&gt;/userdefined/High Color/IMAQdx.ctl"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_Vision_Acquisition_Software.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/driver/NI_Vision_Acquisition_Software.lvlib"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Search Unsorted 1D Array Core.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Helpers/Search Unsorted 1D Array Core.vim"/>
				<Item Name="Search Unsorted 1D Array.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Search Unsorted 1D Array.vim"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="imaq_data.ctl" Type="VI" URL="../../../../../awake project folder/awakebn-finallo-awk-camera-benchmark_SINGLE/awk-camera-benchmark_SINGLE/src/LabVIEW/RT/controls/imaq_data.ctl"/>
			<Item Name="niimaqdx.dll" Type="Document" URL="niimaqdx.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nivissvc.dll" Type="Document" URL="nivissvc.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
